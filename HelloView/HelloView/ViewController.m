//
//  ViewController.m
//  HelloView
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/3/12.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    count = 0;
    _labelArea.text=[NSString stringWithFormat:@"%d", count];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonClick:(id)sender {
    count++;
    _labelArea.text=[NSString stringWithFormat:@"%d",count];
}
@end
